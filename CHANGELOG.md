## 0.2.0 (2022-12-05)

### Feat

- **On-Call.md**: Move date to summary
- **Pairing-Sessions.md**: Move date to summary
- **SemVer**: Implement semantic versioning and conventional changelog

## 0.1.0 (2022-11-06)

### Feat

- **focus time.md**: add Focus Time issue template
- **on-call.md**: add On-Call issue template
- **pairing sessions.md**: add pairing session issue template
- **greenhouse.md**: add Greenhouse issue template for efficiency

### Fix

- **pairing sessions.md**: fix time estimate
